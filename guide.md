Copyright Daniel Hakimi 2016-2017 and contributors. *This guide is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International license](https://creativecommons.org/licenses/by-sa/4.0/).*

This guide is designed to provide simple, sevarable tidbits of advice. You can read any tip here in any order, and they should each, individually, be helpful.

# I. Hats, Glasses, and other Head Stuff

**One.** NEVER wear a **Fedora**. You are not Bruno Mars. Fedoras are *generally* a telltale sign that you are a stereotypical nerdy redditor or something like that. Don't do it. Don't you fucking do it.

**Two.** Probably don't wear a hat ever. This is a rule that you can break if you actually know what you're doing. But if you're reading this guide, you probably don't know what you're doing, huh? If you're not in a baseball stadium, and it's not Halloween, and you aren't Jewish, you probably shouldn't be wearing a hat. Seriously, do you want to look like [this](http://orig09.deviantart.net/be0c/f/2010/170/e/c/id__fedora_and_vest_by_only_sissies_write.jpg)? or like [this](http://25.media.tumblr.com/tumblr_ma9pe9QUUf1ru44ono1_500.jpg)?

# II. Shirts

**One.** "**OCBD**" stands for [Oxford Cloth Button Down](https://cdn.shopify.com/s/files/1/0576/8225/products/SH1-285_TheHillSideSelvedgeOxfordClothBlueShirt_X2.jpeg?v=1423263774), which basically means Casual Button Down. These can be good ideas. Go for it -- just make sure they fit. A white one is going to be your bread and butter.

**Two.** Patterns (for dress shirts and button downs)
- Solid shirts in white and light blue are always good.  
- When you see "**Check**" think "checkers," not "check marks." The **[gingham check](https://www.google.com/search?q=gingham+check+shirt&safe=off&pws=0&source=lnms&tbm=isch&sa=X&ved=0ahUKEwi17cvmvuXRAhXB7SYKHcP6CHgQ_AUICSgC&biw=1536&bih=763)** is pretty legit. There are also [graph](http://farm3.static.flickr.com/2452/3770494020_4ddace5277.jpg) and [tattersall](http://farm3.static.flickr.com/2509/3770494424_5c667ebb12.jpg) checks, which are thinner lines on a solid background.  
- [Plaid](https://uniqlo.scene7.com/is/image/UNIQLO/goods_15_401852?$pdp-medium$) is generally a casual pattern, known for its use on **[flannels](https://cdnd.lystit.com/photos/9955-2015/08/03/uniqlo-blue-men-flannel-check-long-sleeve-shirt-product-0-623126351-normal.jpeg)**.  
- **Stripes** range from the [thick](https://bergbergstore.com/media/catalog/product/2/0/20160301-img_7155.jpg) to the [thin](https://www.indochino.com/images/productimages/s/760x570/1347373146.665230768.additional.01.original.jpg). Thin's probably better.

**Three.** If you have a button down that doesn't fit very well, **throw a sweater over it.** It'll look good enough.

**Four.** If you don't want to tuck your button-down in, make sure it fits right.

**Five.** **Undershirts** are underwear. If they're peeking out of your sweater or shirt, that's kind of weird, dude. V-neck undershirts solve this problem. Also, gray undershirts are probably less visible than white ones under a dress shirt.

# III. Suits

**One.** Lapels. **To be filled in**.

**Two.** Buttons. *Never* button the bottom button on your suit jacket, blazer, or vest... unless there's only one. The rule for a three button suit when standing is "sometimes, always, never" from top to bottom. For a two button suit, it's "always, never." Unbutton when you sit down. There is no such thing as a four button suit. There is one exception to this rule: if it's *super* windy out, the bottom button helps keep everything in place.

**Three.** When you first get your suit, you gotta rip some seams. If there are vents in the back, there is probably a single seam connecting them so they don't flap in shipping. You want to cut that. There might also be a tag on the sleeve -- you obviously don't want tags, so yes, rip that off (gently). You *can* open your pockets up, but you don't really *have to* unless you plan on using a pocket square.

**Four.** Tailoring is a good idea. Get your suit tailored. Do it.

# IV. Black Tie / Eveningwear

**One.** A **tuxedo** isn't really the same as a suit. They're built the same way, and some of the same rules apply, but the reason you wear them is different. A tuxedo is a flashy thing you wear for a party. If the event is "black tie," a suit is weird and boring.

**Two.** You do not wear a belt with a tux. You don't do it. You wear either suspenders, or a cummerbund, or a waistcoat (vest). You pick *one*. You *do not wear a belt*.

**Three.** Lapels: you want either a peak or shawl lapel. A notch lapel is boring and suitlike. You don't want your tux to be boring, you want it to be great. Peak is more traditional, shawl is a little flashy.

# V. Ties, Scarves, and other body accessories

**Specific Guides**: [Ties](/ties.md)

**One.** **Tie length.** Your tie should probably just barely touch your belt. Maybe a little shorter, maybe a tiny bit longer, but that's the rule of thumb. [Basically](http://howtotieatiehq.org/wp-content/uploads/2014/12/what-length-tie.jpg)

**Two.** The **pattern** on your tie has to get along with the pattern on your shirt. If you want to wear an interesting tie, wear a boring shirt, or else they'll clash. See [this infographic](https://www.beckettsimonon.com/blogs/news/7814175-how-to-match-shirt-and-tie-patterns) for some details.

**Three.** If you have to read this guide, and you don't want people to hate you, do not wear a **bracelet.** They're kinda **douchey**, most of the time.

**Four.** Your tie width should match the width of your suit's lapel. See [this crude diagram](http://www.out.com/sites/out.com/files/2015/05/29/tiewidthlayout_-750x-2.jpg).

**Five.** Suspenders are great, but a lot better if you're wearing a blazer or suit or tux to cover 'em up. DO NOT, under any circumstances, wear both suspenders and a belt.

# VI. Pants and Belts and shit

**One.** DO NOT wear **[cargo shorts](http://i0.kym-cdn.com/entries/icons/original/000/021/037/cargo_shorts.jpg)**. Don't fucking do it. This one is not optional. If you're in construction, you'll wear longer pants; if you're not, you don't need all those fucking pockets, they look bad *and* they make you look like a tool. Don't you fucking wear cargo shorts. No.

**Two.** As with everything, make sure your jeans fit properly. **They shouldn't be baggy.** Nobody should be seeing your damn underwear.

**Three.** **Shorts** should probably stop at the knee.

**Four.** Belts should match shoes. Leather should match leather. Black shoes, black belt. Brown shoes, brown belt. Blue shoes... Well, if you're wearing blue shoes, you should know what you're doing.

# VII. Shoes and Socks

**One.** Don't wear **socks with sandals**. (Warning: Link NSFL) [It looks like this](https://s2yimageconsulting.files.wordpress.com/2010/08/socks1.jpg). Sorry I had to show you that. I know, I know, it was terrible. But now you know. Probably don't wear socks with boat shoes either.

**Two.** Don't wear "**dad shoes.**" Basically -- you know those shoes that are kinda sneakers but also kind of look like they're trying to be nice shoes that aren't sneakers? Like [this shit](http://i.ebayimg.com/00/$\(KGrHqVHJBsFIEvvpF48BSB3E!KrCg~~_32.JPG)? Yeah no they look like they're just bad sneakers, it's terrible. If you want to buy cool sneakers, buy cool sneakers. If you want to buy nice shoes, buy nice shoes. Don't try to buy them both in the same item, it doesn't work.

**Three.** **White Socks** are probably a bad idea. Fine for a workout, or whatever, but otherwise they just look bad. See them with sandals above.

# VIII. Final Notes.

**One.** If, by the time you've finished reading this guide, your closet is empty, then go become [the basic bastard](https://www.reddit.com/r/malefashionadvice/comments/5da1dc/the_basic_bastard_basic_wardrobe_and_inspiration/?st=ivl4t7q9&sh=cd5c007f).