# 1. Material.

Silk. This is always good. Yes, this is a simplification. If you want to learn about when other materials are good, go somewhere else. But silk is never a bad material. So if you don't want to read any more on this topic, that's all you need to know: ties should be made of silk.

# 2. How to wear. 

* Use a half windsor knot. Know how to tie a half windsor knot. It is basically always appropriate. Other knots are like other materials -- you don't need to worry about them.
* Your Tie's width (at its widest point) should match your jacket's lapel width (at its widest point). If you're not wearing a jacket, whatever just don't go too skinny or too wide.
* The tip of the tie should just about touch your belt. Shouldn't go past (at least not by far), shouldn't fall short (at least not by far).
* Do not wear a pocket square that looks like your tie. That's bad.
* The more complex the pattern on your tie, the simpler your shirt has to be. Then again, simple tie + simple shirt is fine too.

# 3. Some styles & examples.

Each section will be sorted from cheap to expensive.

# A. Power Ties (stripes).

Power ties are simple, strong colored ties. They are a staple in your closet. This is what you wear to an interview. This is what you wear to court as an attorney. This is what you wear when you want to come off as strong and professional. Read: I didn't say "wear this to work every day." It's "wear it on the days where you want to seem strong." If you want to seem like you're a team player, or something like that, go with simple patterns (D).

They use a simple pattern -- generally either stripes, or no pattern at all. Blue and Red are generally predominant colors. A good rule of thumb is, "would a presidential candidate wear something like this to a debate?" I'm going to list the striped ones first, because I feel like it.

* [Navy with Red and White ($20, The Tie Bar)](https://www.thetiebar.com/product/22602?ResultCount=3)  
* [Red with Black and White ($50, Banana Republic)](http://bananarepublic.gap.com/browse/product.do?cid=1074162&vid=1&pid=586190032)  
* [Navy with white ($50, Banana Republic)](http://bananarepublic.gap.com/browse/product.do?cid=1074162&vid=1&pid=487379012)

# B. Power Ties (solid).

Generally similar to category A.

* [Light Blue ($20, The Tie Bar)](https://www.thetiebar.com/product/31525SK?ResultCount=2). They call this "sound wave herringbone," but the pattern is so subtle that it's practically solid.
* [Burgundy ($20, The Tie Bar)](https://www.thetiebar.com/product/30825tw?ResultCount=5)
* [Blue textured ($50, Banana Republic)](http://bananarepublic.gap.com/browse/product.do?cid=1077773&vid=1&pid=487297002)

# C. Solid Black

This is its own category for a reason. It's here for super formal occasions. Don't wear this to work. There are two options, but anything in between is fine -- just try not to go with any pattern, even subtle, or even a texure, or anything like that. The simpler the better.

* [Tie Guys/Amazon Smile $11.20](https://smile.amazon.com/100-SILK-Solid-BLACK-NeckTie/dp/B000YQ5YA6). This is the cheap one. Get it if you don't have one and can't afford a really nice one. Keep it in your closet for... Well, a funeral.
* [David Donahue, $115](https://www.daviddonahue.com/shop/product_detail.php?ourl=products&style=NT11100002). This is the other end of the spectrum. Buy this if you have a super fancy formal affair coming up and you are going to be around people who can tell the difference and think better of you for spending over ten times as much on your tie. So, yeah, probably unnecessarily expensive.

# D. Simple Patterns

Like the power ties, these are work appropriate, although probably not good enough for interviews. Some of these ties are more complex than others. The more complex the tie, the more simple everything else should be.

* [Blue with dots ($20, The Tie Bar)](https://www.thetiebar.com/product/31009sk?ResultCount=3). Classic, simple, can't really go wrong.
* [Brown Plaid ($20, The Tie Bar)](https://www.thetiebar.com/product/30290sk?ResultCount=2)
* [Lavender Check ($20, The Tie Bar)](https://www.thetiebar.com/product/31616sk?ResultCount=4)
* [Blue Paisley ($25, Ties.com)](http://www.ties.com/ties.com-gable--blue-tie)
* [Purple Textured ($50, Banana Republic)](http://bananarepublic.gap.com/browse/product.do?cid=1074162&vid=1&pid=487297012)
* [Navy + White Stripe (£125, Drakes)](https://www.drakes.com/ties/navy-white-block-stripe-silk-jacquard-tie). Note: I wouldn't call this a power tie. There's too much white. You might disagree, but what do you know? You're reading a remedial necktie guide.

# E. Fun patterns

Don't wear these to work unless you want to be "that guy" who dresses fancy at the office. Maybe wear them to some weddings, or stuff like that.

These ties will clash dramatically with any interesting shirt pattern. Probably best to wear them with a plain white button down.

* [Pink + Blue ($20, The Tie Bar)](https://www.thetiebar.com/product/30298sk?ResultCount=2)
* [Red, White and Blue check ($25, Ties.com)](http://www.ties.com/ties.com-bora-bora--red-tie)
* [Floral ($25, SprezzaBox)](https://www.sprezzabox.com/products/navy-bouquet-tie-brooklyn). Note that this is cotton. Just thought it was worth looking at.
* [Pink + Blue Paisley ($50, Express)](http://www.express.com/clothing/men/paisley-narrow-silk-tie/pro/04585000/cat1920074?selectedColor=PINK) (side note: this tie looks a lot better online than in person) 
* [White elephant and leaf print (£125, Drakes)](https://www.drakes.com/ties/white-elephant-leaf-print-silk-tie)
* [Navy Tile Print (£125, Drakes)](https://www.drakes.com/ties/navy-tile-print-36oz-foulard-silk-tie)
