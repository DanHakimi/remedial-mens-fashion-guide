See [guide.md](/guide.md), which should link to more specific guides as appropriate.

Licensed under [CC-BY-SA-4.0](/license.txt).